@extends('layouts.app')

@section('title', 'Candidates')

@section('content')

<h1>List of users</h1>

<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Role</th> 
        @can ('edit-user')
            <th>edit</th>
        @endcan
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>
            </td>
            @can ('edit-user')
            <td><a href = "{{route('users.edit',$user->id)}}">Edit</a></td>
            @endcan
        </tr>
    @endforeach
</table>
@endsection

